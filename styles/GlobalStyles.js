import { createGlobalStyle } from 'styled-components'

export const Global = createGlobalStyle`

@font-face {
  font-family: "Gilroy-ExtraBold";
  src: url("/fonts/Gilroy-ExtraBold/font.woff");
  font-style: bold;
  font-weight: 900;
  font-display: swap;
}
@font-face {
  font-family: "Gilroy-Light";
  src: url("/fonts/Gilroy-Light/font.woff");
  font-style: normal;
  font-display: swap;
}
*, *:before, *:after {
  box-sizing: border-box;
  padding: 0;
  margin: 0;
  list-style: none;
}
html{
  font-size: 62.5%;

  @media ${({ theme }) => theme.breakpoints.tablet}{
    font-size: 50%;
  }
}

  html,
  body {
  font-family: ${({ theme }) => theme.fonts.primary};

}

a {
  color: inherit;
  text-decoration: none;
}
`
