export default {
  colors: {
    yellow: '#ffd000',
    orange: '#ff6600',
    fontPrimary: '#292929',
    fontSecundary: '#3d3d3d',
    lightGrey: '#d1d1d1',
    lightGrey2: '#9b9b9b'
  },
  fonts: {
    primary: 'Poppins, sans-serif, Roboto',
    secundary: 'Rubik, sans-serif, Roboto',
    logo: 'Gilroy-ExtraBold',
    heroSub: 'Gilroy-Light'
  },
  breakpoints: {
    notebook: 'screen and (max-width: 1280px)',
    tablet: 'screen and (max-width: 1024px)',
    mobile: 'screen and (max-width: 750px)',
    mobile2: 'screen and (max-width: 520px)'
  }
}
