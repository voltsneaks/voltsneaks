import { CartIcon, Logo } from 'components/atoms'
import { MobileMenu, NavMenu } from 'components/molecules'
import { CartWrapper, LogoWrapper, MenuIcon, MenuWrapper, NavDiv, NavWrapper } from './Nav.elements'
import { MdMenu, MdClear } from 'react-icons/md'
import { useState } from 'react'
import { Bolsa } from '..'

const Nav = ({ carrito }) => {
  const [click, setClick] = useState(false)
  const [openCart, setOpenCart] = useState(false)

  return (
      <>
      <NavDiv>
        <MobileMenu click={click} setClick={setClick}/>
        <NavWrapper>
          <MenuIcon onClick={() => setClick(!click)}>
          <MdMenu size="2.5rem" color="#292929" />
          </MenuIcon>
          <LogoWrapper>
            <Logo />
          </LogoWrapper>
          <MenuWrapper>
            <NavMenu/>
          </MenuWrapper>
          <CartWrapper onClick={() => setOpenCart(!openCart)}>
            <CartIcon info={carrito.length} />
          </CartWrapper>
        </NavWrapper>
        <Bolsa openCart={openCart} setOpenCart={setOpenCart}/>
      </NavDiv>
      </>
  )
}

export default Nav
