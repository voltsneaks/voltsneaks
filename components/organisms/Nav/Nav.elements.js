import styled from 'styled-components'

export const NavDiv = styled.div`
  height: 10rem;
  position: relative;
  width: 100%;
  background-color: white;
  z-index: 100;
  
  @media ${({ theme }) => theme.breakpoints.mobile}{
    height: 7rem;
  }
`

export const NavWrapper = styled.nav`
  background-color: white;
  margin: 0 auto;
  padding: 0 100px;
  width: 100%;
  height: 10rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 4rem;
  position: fixed;
  z-index: 999;
  animation: fadein 2s;

@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
@media ${({ theme }) => theme.breakpoints.tablet}{
    padding: 0 2rem;
  }
@media ${({ theme }) => theme.breakpoints.mobile}{
    height: 7rem;
  }
`

export const MenuIcon = styled.div`
  display: none;

  @media ${({ theme }) => theme.breakpoints.tablet}{
    display: flex;
  }
`

export const LogoWrapper = styled.div`
  display: flex;
  justify-content: start;
  width: 15rem;

  @media ${({ theme }) => theme.breakpoints.tablet}{
    justify-content: flex-end;
  }
`

export const MenuWrapper = styled.div`
display: block;

  @media ${({ theme }) => theme.breakpoints.tablet}{
    display: none;
  }
`

export const CartWrapper = styled.div`
  width: 15rem;
  display: flex;
  justify-content: flex-end;
  align-items: center;

  @media ${({ theme }) => theme.breakpoints.tablet}{
  width: 5rem;
  }
`

export const AccountWrapper = styled.div`
  margin-top: .5rem;
  margin-right: 1.5rem;

  @media ${({ theme }) => theme.breakpoints.tablet}{
    display: none;
  }
`

export const DropMenu = styled.div`
  width: 100%;
  height: 9rem;
  background-color: ${({ theme }) => theme.colors.orange};
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  position: absolute;
  left: 0;
  padding: 6.4rem 10rem 0rem 10rem ;
  transform: ${({ open }) => open ? 'translateY(60%)' : 'translateY(-60%)'};
  opacity: ${({ open }) => open ? '1' : '0'};
  transition: opacity 300ms ease-in-out, transform 300ms ease-in-out;

    p{
      color: white;
    }
`
