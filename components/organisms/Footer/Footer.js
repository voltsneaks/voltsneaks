import { Text } from 'components/atoms'
import { FooterBody, FooterWrapper, FooterContent, FooterInfo, FooterLogo, FooterAnchor, DecoLine } from './Footer.elements'

const Footer = () => {
  return (
        <FooterWrapper>
          <FooterBody>
            <FooterContent>
              <FooterLogo>VOLTSNEAKS</FooterLogo>
            <FooterInfo>
             <FooterAnchor href="/terminosyCondiciones">Terminos y Condiciones</FooterAnchor>
             <Text color="white" size="xSmall" weight="bold">/</Text>
             <FooterAnchor href="/contacto">Contacto</FooterAnchor>
            </FooterInfo>
            <Text color="white" size="small">Copyright © 2020</Text>
            </FooterContent>
          </FooterBody>
            <DecoLine />
        </FooterWrapper>
  )
}

export default Footer
