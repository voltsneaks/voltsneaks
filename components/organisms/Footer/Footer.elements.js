import styled from 'styled-components'
import theme from 'styles/theme'

export const FooterWrapper = styled.footer`
  width: 100%;
  height: 14rem;
  background-color: ${({ theme }) => theme.colors.orange};
  margin-top: 5rem;
`

export const FooterBody = styled.div`
  max-width: 1440px;
  padding: 0 5rem 0 5rem;
  margin: 0 auto;
  display: flex;
  justify-content: start;
  align-items: flex-end;

  @media ${({ theme }) => theme.breakpoints.mobile}{
  padding: 0 5rem 0 2.5rem;
  }
`

export const FooterContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 23rem;
  height: 12rem;
  padding-top: 1rem;
`

export const FooterInfo = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-bottom: .8rem;
`

export const FooterLogo = styled.p`
  font-size: 3.5rem;
  font-family: ${({ theme }) => theme.fonts.logo};
  color: white;
  letter-spacing: -.15rem;
  margin-bottom: .7rem;
`

export const FooterAnchor = styled.a`
  font-weight: bold;
  font-size: 1.2rem;
  color: white;
`

export const DecoLine = styled.hr`
  display: block;
  border-style: inset;
  border-width: 100%;
  border-color: white;
`
