import { Text } from 'components/atoms'
import { CartItem } from 'components/molecules'
import { useData } from 'contexts/DataProvider'
import Link from 'next/link'
import { Submit, SubmitDisabled } from '../ProductDetails/ProductDetails.elements'
import { BolsaWrapper, ItemsWrapper, BolsaCloseIcon, BolsaControlsWrapper, BolsaCheckoutWrapper, BolsaTotalWrapper } from './Bolsa.elements'

const Bolsa = ({ openCart, setOpenCart }) => {
  const { carrito, eliminarProd } = useData()

  const total = carrito.reduce((sum, item) => {
    return sum + parseFloat(item.precio)
  }, 0)

  return (
          <BolsaWrapper openCart={openCart}>
            <BolsaControlsWrapper>
              <Text weight="bold" size="large">Bolsa</Text>
              <BolsaCloseIcon onClick={() => setOpenCart(!openCart)}/>
            </BolsaControlsWrapper>
            <ItemsWrapper>
              {
                carrito.map((prod) => (
                  <CartItem
                  {...prod}
                  eliminarProd={eliminarProd}
                  />
                ))
              }
            </ItemsWrapper>
            <BolsaCheckoutWrapper>
              <BolsaTotalWrapper>
                <Text size="large">Total</Text>
                <Text size="large">${total}{total === 0 ? null : 0}</Text>
              </BolsaTotalWrapper>
              <Link href="/checkout">
              {
                carrito.length === 0
                  ? <SubmitDisabled padding="2rem" onClick={() => setOpenCart(!openCart)}>COMPRAR</SubmitDisabled>
                  : <Submit padding="2rem" onClick={() => setOpenCart(!openCart)}>COMPRAR</Submit>
              }
              </Link>
            </BolsaCheckoutWrapper>
          </BolsaWrapper>
  )
}

export default Bolsa
