import styled from 'styled-components'
import { MdClear } from 'react-icons/md'

export const BolsaWrapper = styled.div`
  width: 45rem;
  padding: 3rem 0;
  position: fixed;
  right: 0;
  top: 0;
  height: 100vh;
  background-color: white;
  z-index: 999;
  transform: ${({ openCart }) => openCart ? 'translateX(0%)' : 'translateX(100%)'};
  transition: transform 200ms ease-in-out;

  @media ${({ theme }) => theme.breakpoints.mobile}{
    width: 100%;
  }

`
export const ItemsWrapper = styled.div`
  width: 90%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`
export const CheckoutWrapper = styled.div`
  width: 30%;
  display: flex;
  flex-direction: column;
  border: 1px solid grey;
`
export const BolsaControlsWrapper = styled.div`
  width: 90%;
  padding: 1rem;
  margin: 0 auto 1.2rem auto;
  display: flex;
  justify-content: space-between;
`

export const BolsaCloseIcon = styled(MdClear).attrs({ size: '2.5rem' })`
  color: ${({ theme }) => theme.colors.fontPrimary};
  cursor: pointer;
`

export const BolsaCheckoutWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  flex-direction: column;
  width: 90%;
  height: 20rem;
  padding: 1rem;
  margin: 5rem auto;
  border-top: 1.5px solid ${({ theme }) => theme.colors.fontPrimary};
  
`
export const BolsaTotalWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`
