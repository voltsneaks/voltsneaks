import { Text } from 'components/atoms'
import { useData } from 'contexts/DataProvider'
import { useState } from 'react'
import { NumerosWrapper, DetailsInfo, DetailsWrapper, Select, FormWrapper, Submit, ButtonDisabled } from './ProductDetails.elements'

const ProductDetails = ({ ...product }) => {
  const [numero, setNumero] = useState('--')

  const productToCart = {
    marca: product.marca.nombre,
    modelo: product.modelo,
    categoria: product.categorias[1].nombre,
    precio: product.precio,
    id: product.id + numero,
    cantidad: product.cantidad,
    imagen: product.imagenes[0].formats.small.url,
    talla: numero
  }

  const { agregarProd } = useData()

  const handleSubmit = (e) => {
    e.preventDefault()
    agregarProd(productToCart)
  }

  return (
        <DetailsWrapper>
            <DetailsInfo>
              <Text size="xLarge">{product.marca.nombre}</Text>
              <Text weight="bold" margin="1rem 0 2rem 0" size="xLarge">{product.modelo}</Text>
              <Text >{product.categorias[1].nombre}</Text>
              <Text size="xxLarge" margin="2rem 0">${product.precio}</Text>
            </DetailsInfo>
        <FormWrapper onSubmit={handleSubmit}>
        {
          product.disponible
            ? (<NumerosWrapper>
             <Text size="large" margin="0 2rem 0 0">Tallas:</Text>
             <Select onChange={(e) => setNumero(e.target.value) } >
             <option selected="selected"
               >--</option>
             {
               product.numeros.map((num) => (
               <option
               >{num.numero}</option>
               ))
             }
             </Select>
             </NumerosWrapper>)
            : <Text size="large" margin="2rem 0">Producto agotado</Text>
        }
        {
          numero === '--'
            ? <ButtonDisabled padding="2rem 1rem" width="100%" disabled>AGREGAR A LA BOLSA</ButtonDisabled>
            : <Submit padding="2rem 1rem" width="100%">AGREGAR A LA BOLSA</Submit>
        }
        </FormWrapper>
        <Text size="xLarge" margin="2.5rem 0 0 0">Descripcion</Text>
        <Text>{product.descripcion}</Text>
        <Text size="xLarge" margin="2.5rem 0 0 0">Envios</Text>
        <Text>Realizamos envios para todo el territorio nacional a travez de ChileExpress, los valores varian segun cantidad de productos a enviar y ciudad de destino.</Text>
        </DetailsWrapper>
  )
}

export default ProductDetails
