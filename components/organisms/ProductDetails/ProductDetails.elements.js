import { ButtonSubmit } from 'components/atoms'
import styled from 'styled-components'
import theme from 'styles/theme'

export const DetailsWrapper = styled.div`
  width: 40%;
  height: 70rem;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;

  @media ${theme.breakpoints.mobile}{
    width: 95%;
    margin: 0 auto;
  }
`

export const DetailsInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`
export const NumerosWrapper = styled.div`
  display: flex;
  flex-flow: wrap;
  align-items: center;
  justify-content: flex-start;
  margin-bottom: 2.5rem;
`

export const Select = styled.select`
  color: ${({ theme }) => theme.colors.fontPrimary};
  padding: ${({ padding }) => padding || '.3rem 1rem'};
  width: ${({ width }) => width || null};
  margin: ${({ margin }) => margin || null};
  border: 1px solid ${({ theme }) => theme.colors.fontPrimary};
  background-color: white;
  font-size: 1.6rem;
  font-weight: 500;  
  border-radius: .2rem;
`
export const FormWrapper = styled.form`
  
`
export const Submit = styled(ButtonSubmit)`
  color: white;
  background-color: ${({ theme }) => theme.colors.fontPrimary};

  &:hover{
        background-color: ${({ theme }) => theme.colors.orange};
        color: white;
        border-color: transparent;
    }

`
export const SubmitDisabled = styled(ButtonSubmit).attrs({ disabled: true })`
  color: white;
  background-color: ${({ theme }) => theme.colors.fontPrimary};
  :disabled{
    opacity: 0.4;
    pointer-events: none;
  }
`
export const ButtonDisabled = styled.button`
  color: white;
  border-color: ${({ theme }) => theme.colors.fontPrimary};
  padding: ${({ padding }) => padding || '.3rem 1rem'};
  width: ${({ width }) => width || null};
  margin: ${({ margin }) => margin || null};
  text-align: center;
  font-size: 1.6rem;
  font-weight: 500;  
  border: 1px solid ${({ theme }) => theme.colors.fontPrimary};
  background-color: ${({ theme }) => theme.colors.fontPrimary};
  border-radius: .2rem;
  pointer-events: none;
  letter-spacing: -.5px;

  :disabled{
    opacity: 0.4;
  }

`
