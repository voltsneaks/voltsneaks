import styled from 'styled-components'

export const Grid = styled.div`
  margin: 1rem 0 10rem 0;
  width: 100%;
  display: grid;
  grid-auto-columns: 1fr;
  grid-row-gap: 3rem;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: auto;
  grid-column-gap: 2rem;

    @media ${props => props.theme.breakpoints.tablet}{
      grid-row-gap: 2rem;
      grid-template-columns: 1fr 1fr;
      grid-column-gap: 1rem;
    }

    @media screen and (max-width: 768px){
      grid-template-columns: 1fr 1fr;
      grid-column-gap: .5rem;
    }
`
