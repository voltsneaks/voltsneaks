import { useData } from 'contexts/DataProvider'
import { Grid } from './Productos.elements'
import { Card } from 'components/molecules'
import { useState, useEffect } from 'react'
import { trackWindowScroll } from 'react-lazy-load-image-component'

const Productos = ({ category, index }) => {
  const { data } = useData()
  const [info, setInfo] = useState([])

  const marcas = data.filter((prod) => prod.marca.nombre === index)
  const calzado = data.filter((prod) => prod.categorias[0].nombre === index)
  const training = data.filter((prod) => prod.categorias[1].nombre === 'Training')
  const accesorios = data.filter((prod) => prod.categorias[1].nombre === index)

  useEffect(() => {
    const filter = () => {
      switch (category) {
        case 'calzado':
          return setInfo(calzado)
        case 'marcas':
          return setInfo(marcas)
        case 'training':
          return setInfo(training)
        case 'accesorios':
          return setInfo(accesorios)
        default:
          return null
      }
    }
    filter()
  }, [data])

  return (
        <Grid>
          {info.map((products) => (
            <Card {...products}/>
          ))}
        </Grid>
  )
}

export default trackWindowScroll(Productos)
