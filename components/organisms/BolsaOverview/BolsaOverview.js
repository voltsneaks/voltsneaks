import { Text } from 'components/atoms'
import { CartItem } from 'components/molecules'
import { useData } from 'contexts/DataProvider'
import { BolsaWrapper, ItemsWrapper, BolsaDetailsWrapper, BolsaControlsWrapper, BolsaCheckoutWrapper, BolsaTotalWrapper } from './BolsaOverview.elements'

const BolsaOverview = ({ openCart }) => {
  const { carrito, eliminarProd } = useData()

  const total = carrito.reduce((sum, item) => {
    return sum + parseFloat(item.precio)
  }, 0)

  return (
          <BolsaWrapper openCart={openCart}>
            <BolsaControlsWrapper>
              <Text weight="bold" size="large">Bolsa</Text>
            </BolsaControlsWrapper>
            <ItemsWrapper>
              {
                carrito.map((prod) => (
                  <CartItem
                  {...prod}
                  eliminarProd={eliminarProd}
                  />
                ))
              }
            </ItemsWrapper>
            <BolsaCheckoutWrapper>
                <Text size="large" margin="1rem 0">Detalle</Text>
              <BolsaDetailsWrapper>
                <Text>Productos</Text>
                <Text>${total}{total === 0 ? null : 0}</Text>
              </BolsaDetailsWrapper>
              <BolsaDetailsWrapper>
                <Text>Envio</Text>
                <Text>$5.000</Text>
              </BolsaDetailsWrapper>
              <BolsaTotalWrapper>
                <Text size="large">Total</Text>
                <Text size="large">${total + 5.000}{total === 0 ? null : 0}</Text>
              </BolsaTotalWrapper>
            </BolsaCheckoutWrapper>
          </BolsaWrapper>
  )
}

export default BolsaOverview
