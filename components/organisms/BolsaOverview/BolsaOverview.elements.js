import styled from 'styled-components'

export const BolsaWrapper = styled.div`
  width: 100%;
  background-color: white;
  margin-top: .5rem;

  @media ${({ theme }) => theme.breakpoints.mobile}{
    width: 100%;
  }

`
export const ItemsWrapper = styled.div`
  width: 90%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`
export const CheckoutWrapper = styled.div`
  width: 30%;
  display: flex;
  flex-direction: column;
  border: 1px solid grey;
`
export const BolsaControlsWrapper = styled.div`
  width: 90%;
  padding: 1rem;
  margin: 0 auto 1.2rem auto;
  display: flex;
  justify-content: space-between;
`

export const BolsaCheckoutWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
  width: 90%;
  height: 20rem;
  padding: 1rem;
  margin: 1rem auto;
  
`
export const BolsaDetailsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 1rem;
`
export const BolsaTotalWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 1rem;
`
