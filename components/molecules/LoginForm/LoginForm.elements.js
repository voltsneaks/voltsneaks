import styled from 'styled-components'

export const LoginWrapper = styled.form`
  margin: 14rem auto;
  width: 40rem;
  height: 25rem;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  align-items: center;
`

export const Input = styled.input`
  width: 100%;
  padding: .8rem;
  margin-bottom: 1.5rem;
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
  border-radius: 2px;
  font-size: 1.6rem;
`
