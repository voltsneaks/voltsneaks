// import { Anchor, ButtonSubmit, LogAnchor, Text } from "components/atoms"
// import { useAuth } from "contexts/AuthProvider"
// import { useForm } from "react-hook-form"
// import { LoginWrapper, Input } from "./LoginForm.elements"

// const LoginForm = () => {

//     const { login, authError } = useAuth()

//     const { register, handleSubmit, currentUser } = useForm()

//     const onSubmit = (values) => {
//         const {email, password} = values
//         login(email, password)
//     }

//     return (
//         <LoginWrapper onSubmit={handleSubmit(onSubmit)}>
//             {currentUser && <Text color="red" size="xSmall">{currentUser.email}</Text>}
//             <Text size="xLarge" margin="0 0 3rem 0" weight="bold" spacing="-.14rem">ACCEDE A TU CUENTA</Text>
//             {authError && <Text color="red" size="xSmall">{authError}</Text>}
//             <Input type="email"
//             placeholder="Email"
//             name="email"
//             ref={register}
//             />
//             <Input type="password"
//             placeholder="Contraseña"
//             name="password"
//             ref={register}
//             />
//             <ButtonSubmit login width="100%" padding="1rem">Acceder</ButtonSubmit>
//             <Text margin="1.2rem 0 .5rem 0" size="small">¿Aun no tienes una cuenta?</Text><Anchor bold size="small" decoration="underline" href="/registration">Crear Cuenta</Anchor>
//         </LoginWrapper>
//     )
// }

// export default LoginForm
