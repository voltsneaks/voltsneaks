/* eslint-disable react/no-unescaped-entities */
import Link from 'next/link'
import { HeroImage, HeroWrapper, HeroTitleWrapper, HeroTitle, HeroAnchor } from './Hero.elements'

const Hero = () => {
  return (
      <HeroWrapper>
          <HeroTitleWrapper>
            <HeroTitle>LeBron 18 <br/> "Los Angeles By Night"</HeroTitle>
            <Link href="/details/5fd966b148bcc26ff9de92f7">
            <HeroAnchor>Ver Mas</HeroAnchor>
            </Link>
          </HeroTitleWrapper>
            <HeroImage src="/HeroXL3.webp"/>
      </HeroWrapper>
  )
}

export default Hero
