import styled from 'styled-components'

export const HeroWrapper = styled.div`
  width: 100%;
  position: relative;
  overflow: hidden;

`

export const HeroImage = styled.img`
  width: 100%;
  display: block;
  
  @media ${({ theme }) => theme.breakpoints.mobile2}{
    width: 140%;
    transform: translate(-2rem);
  }
`
export const HeroBodyWrapper = styled.div`
  max-width: 1440px;
  margin: 0 auto;
  display: flex;
  align-items: flex-end;
`

export const HeroTitleWrapper = styled.div`
  width: 37rem;
  height: 12rem;
  position: absolute;
  left: 7%;
  bottom: 20%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  word-wrap: break-word;
  z-index: 99;

   p{
     font-size: 4rem;

     @media ${({ theme }) => theme.breakpoints.tablet}{
     font-size: 2.5rem;

   }
     
      @media ${({ theme }) => theme.breakpoints.mobile}{
     font-size: 1.8rem;
        
    }
    
  }
  
  @media ${({ theme }) => theme.breakpoints.tablet}{
    left: 4%;
    bottom: 15%;
  }
  @media ${({ theme }) => theme.breakpoints.mobile}{
    left: 5%;
    bottom: 1%;
    width: 19rem;
  }

`

export const HeroTitle = styled.p`
  font-size: 3.5rem;
  font-family: ${({ theme }) => theme.fonts.logo};

`

export const HeroAnchor = styled.a`
  color: white;
  text-align: center;
  width: 11rem;
  font-size: 1.6rem;
  font-weight: 500;  
  border: 1px solid transparent;
  border-color: ${({ theme }) => theme.colors.fontPrimary};
  background-color: ${({ theme }) => theme.colors.fontPrimary};
  border-radius: .2rem;
  cursor: pointer;
  padding: 1rem 2rem;
  margin-top: 1.8rem;
  letter-spacing: -.5px;
  transition: background-color 100ms ease-in, border 100ms ease-out, color 100ms ease-in;

    &:hover{
      background-color: ${({ theme }) => theme.colors.orange};
      border-color: transparent;
      color: white;
    }

    @media ${({ theme }) => theme.breakpoints.mobile}{
      padding: .8rem 1rem;
      font-size: 1.2rem;
      width: 8rem;
      margin-top: 1rem;


  }
`
// export const HeroSubTitle = styled.p`
//   margin-top: 1rem;
//   font-family: ${({theme}) => theme.fonts.heroSub};
//   font-size: 1.5rem;

// `;
