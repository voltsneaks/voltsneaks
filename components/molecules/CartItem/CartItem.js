import { Text } from 'components/atoms'
import React from 'react'
import { CartItemWrapper, CartInfoWrapper, CartItemControlsWrapper, CartItemBody, DeleteIcon, CartItemImage } from './CartItem.elements'

const CartItem = ({ aumentar, disminuir, eliminarProd, ...rest }) => {
  return (
      <CartItemWrapper key={rest.id}>
        <CartItemBody>
            <CartItemImage cover width="null" height="100%" src={rest.imagen}/>
          <CartInfoWrapper>
            <Text margin=".5rem 0" size="small">{rest.marca}</Text>
            <Text margin=".5rem 0" size="small">{rest.modelo}</Text>
            <Text margin=".5rem 0" size="small">Talla: US {rest.talla}</Text>
          </CartInfoWrapper>
          </CartItemBody>
        <CartItemControlsWrapper>
          <Text>${rest.precio}</Text>
          <DeleteIcon onClick={() => eliminarProd(rest.id)}>x</DeleteIcon>
        </CartItemControlsWrapper>
      </CartItemWrapper>
  )
}

export default CartItem

{ /* <Button onClick={() => disminuir(rest.id)}>-</Button>
<Text size="xSmall">{rest.cantidad}</Text>
<Button onClick={() => aumentar(rest.id)}>+</Button> */ }
