import styled from 'styled-components'
import { FiTrash2 } from 'react-icons/fi'

export const CartItemWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  width: 100%;
  height: 12rem;
  padding: 1rem;
  border-radius: 3px;
  cursor: pointer;
`
export const CartItemBody = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
`

export const CartInfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin-left: 1.2rem;
`

export const CartItemControlsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: space-between;
  padding: 2rem 0;
`

export const DeleteIcon = styled(FiTrash2).attrs({ size: '1.4rem' })`
  color: ${({ theme }) => theme.colors.fontPrimary};

`
export const CartItemImage = styled.img`
  width: ${props => props.width || '100%'};
  height: ${props => props.width || 'null'};
  cursor: ${({ pointer }) => (pointer ? 'pointer' : null)};
  object-fit: ${({ cover }) => (cover ? 'cover' : null)};
  filter: brightness(100%);
  transition: filter 500ms ease-in-out;
  display: block;
`
