import { Text } from 'components/atoms'
import { useData } from 'contexts/DataProvider'
import { useState } from 'react'
import { useForm } from 'react-hook-form'
import regiones from 'services/regiones'
import { CheckoutSchema } from 'services/validationSchema'
import { yupResolver } from '@hookform/resolvers/yup'
import { CheckoutFormWrapper, CheckoutInput, RegionSelect, InputField, EmailSubmit, InputWrapper, EmailSubmitDisabled, AddressWrapper, NameWrapper, CityWrapper } from './CheckoutForm.elements'

const CheckoutForm = () => {
  const [addressDisabled, setAddressDisabled] = useState(true)

  const { checkoutSend } = useData()

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(CheckoutSchema)
  })

  const onSubmit = (values) => {
    const { email, nombre, apellido, direccion, ciudad, telefono } = values
    return checkoutSend(email, nombre, apellido, direccion, ciudad, telefono)
  }

  return (
      <CheckoutFormWrapper onSubmit={handleSubmit(onSubmit)} noValidate>

          {/* REGION */}
        <InputField >
        <Text size="large" >Region de Envio</Text>
          <RegionSelect onChange={() => setAddressDisabled(false)} >
            {addressDisabled && <option selected="selected">--</option>}
            {
              regiones.regiones.map((reg) => (
              <option>{reg.NombreRegion}</option>
              ))
            }
          </RegionSelect>
        </InputField>

          {/* EMAIL */}
        <InputField disabled={addressDisabled}>
          <Text disabled={addressDisabled} size="large">Email</Text>
            <CheckoutInput
            type="email"
            name="email"
            disabled={addressDisabled}
            ref={register}
            error={errors.email}
            />
        {errors.email && <Text color="tomato" size="small" margin="0 0 0 .5rem">{errors.email.message}</Text>}
        </InputField>

        {/* DIRECCION */}
        <AddressWrapper disabled={addressDisabled}>
          <Text disabled={addressDisabled} size="large">Direccion</Text>
          <NameWrapper>
            <InputWrapper width="48%">
              <CheckoutInput
                type="text"
                name="nombre"
                width="100%"
                placeholder="Nombre"
                disabled={addressDisabled}
                ref={register}
                error={errors.nombre}
                />
                {errors.nombre && <Text color="tomato" size="small" margin=".5rem">{errors.nombre.message}</Text>}
            </InputWrapper>
            <InputWrapper width="48%">
              <CheckoutInput
                type="text"
                name="apellido"
                width="100%"
                placeholder="Apellido"
                disabled={addressDisabled}
                ref={register}
                error={errors.apellido}
                />
                {errors.apellido && <Text color="tomato" size="small" margin=".5rem">{errors.apellido.message}</Text>}
            </InputWrapper>
          </NameWrapper>
            <InputWrapper>
              <CheckoutInput
              type="text"
              name="direccion"
              placeholder="Direccion"
              disabled={addressDisabled}
              ref={register}
              error={errors.direccion}
              />
              {errors.direccion && <Text color="tomato" size="small" margin=".5rem">{errors.direccion.message}</Text>}
              </InputWrapper>
            <CityWrapper>
              <InputWrapper width="48%">
                <CheckoutInput
                type="text"
                name="zip"
                placeholder="Codigo Postal"
                disabled={addressDisabled}
                ref={register}
                error={errors.zip}
                />
                {errors.zip && <Text color="tomato" size="small" margin=".5rem">{errors.zip.message}</Text>}
              </InputWrapper>
              <InputWrapper width="48%">
                <CheckoutInput
                type="text"
                name="ciudad"
                placeholder="Ciudad"
                disabled={addressDisabled}
                ref={register}
                error={errors.ciudad}
                />
                {errors.ciudad && <Text color="tomato" size="small" margin=".5rem">{errors.ciudad.message}</Text>}
              </InputWrapper>
            </CityWrapper>
            <InputWrapper>
              <CheckoutInput
              type="text"
              name="telefono"
              placeholder="Telefono"
              disabled={addressDisabled}
              ref={register}
              error={errors.telefono}
              />
              {errors.telefono && <Text color="tomato" size="small" margin=".5rem">{errors.telefono.message}</Text>}
            </InputWrapper>
          {addressDisabled ? <EmailSubmitDisabled width="100%" padding="2rem" >REALIZAR PAGO</EmailSubmitDisabled> : <EmailSubmit width="100%" padding="2rem" >REALIZAR PAGO</EmailSubmit>}
        </AddressWrapper>
      </CheckoutFormWrapper>
  )
}

export default CheckoutForm
