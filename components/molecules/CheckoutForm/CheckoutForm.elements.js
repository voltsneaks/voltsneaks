import styled from 'styled-components'
import theme from 'styles/theme'

export const CheckoutFormWrapper = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const RegionSelect = styled.select`
  color: ${({ theme }) => theme.colors.fontPrimary};
  padding: ${({ padding }) => padding || '.5rem 1rem'};
  width: ${({ width }) => width || '100%'};
  margin: ${({ margin }) => margin || null};
  border: 1px solid ${({ theme }) => theme.colors.fontPrimary};
  background-color: white;
  font-size: 1.6rem;
  font-weight: 500;  
  border-radius: .2rem;
`

export const CheckoutInput = styled.input.attrs(({ type }) => ({
  type: type
}))`
  color: ${({ theme }) => theme.colors.fontPrimary};
  padding: ${({ padding }) => padding || '.5rem 1rem'};
  width: ${({ width }) => width || '100%'};
  margin: ${({ margin }) => margin || null};
  border: 1px solid ${({ disabled }) => disabled ? theme.colors.lightGrey : theme.colors.fontPrimary};
  background-color: white;
  font-size: 1.6rem;
  font-weight: 500;  
  border-radius: .2rem;

  :disabled{
      
     ::placeholder{
        color: transparent;
    }
  }
`

export const InputField = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  height: 14rem;
  margin: 1.5rem 0;
  padding: 3rem;
  border: 1px solid ${({ disabled }) => disabled ? theme.colors.lightGrey : theme.colors.fontPrimary};

  p:first-child{
    color: ${({ disabled }) => disabled ? theme.colors.lightGrey : theme.colors.fontPrimary};
  }
`
export const AddressWrapper = styled.div`
  width: 100%;
  height: 45rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  margin: 1.5rem 0;
  padding: 3rem 3rem;
  border: 1px solid ${({ disabled }) => disabled ? theme.colors.lightGrey : theme.colors.fontPrimary};

  p:first-child{
    color: ${({ disabled }) => disabled ? theme.colors.lightGrey : theme.colors.fontPrimary};
  }
`

export const EmailSubmit = styled.button`
  color: white;
  border-color: ${({ theme }) => theme.colors.fontPrimary};
  padding: ${({ padding }) => padding || '.3rem 1rem'};
  width: ${({ width }) => width || null};
  margin: ${({ margin }) => margin || null};
  text-align: center;
  font-size: 1.6rem;
  font-weight: 500;  
  border: 1px solid ${({ theme }) => theme.colors.fontPrimary};
  background-color: ${({ theme }) => theme.colors.fontPrimary};
  border-radius: .2rem;
  cursor: pointer;
  letter-spacing: -.5px;
  transition: background-color 200ms ease-in, border 200ms ease-out, color 200ms ease-in;

    &:hover{
        background-color: ${({ theme }) => theme.colors.orange};
        border-color: transparent;
        color: white;
    }
`

export const EmailSubmitDisabled = styled.button`
  color: ${({ theme }) => theme.colors.lightGrey};
  border-color: ${({ theme }) => theme.colors.lightGrey};
  padding: ${({ padding }) => padding || '.3rem 1rem'};
  width: ${({ width }) => width || null};
  margin: ${({ margin }) => margin || null};
  text-align: center;
  font-size: 1.6rem;
  font-weight: 500;  
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
  background-color: white;
  border-radius: .2rem;
  pointer-events: none;
  letter-spacing: -.5px;
`
export const InputWrapper = styled.div`
  width: ${({ width }) => width || '100%'};
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

`

export const NameWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`
export const CityWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`
