import styled from 'styled-components'

export const SlideMenuItem = styled.div`
  position: relative;  

  .icons{
    transform: ${({ open }) => open ? 'rotate(-90deg)' : null};
    transition: all .3s ease;
    }      
`

export const MobileAnchorWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  transition: all 300ms ease-in-out;
`

export const SubMenuAnchor = styled.a`
  color: ${({ theme }) => theme.colors.fontPrimary};
  font-size: ${({ submenu }) => submenu ? '2rem' : '2.5rem'};
  font-weight: ${({ submenu }) => submenu ? '600' : '600'};
  margin-right: 1.8rem;
  margin-top: ${({ submenu }) => submenu ? '.5rem' : '0'};
  padding: ${({ submenu }) => submenu ? '0 0 4rem 0' : '2rem 0'};
  cursor: pointer;
  transition: color 0.2s ease-in-out;

  :active{
      color: ${({ theme }) => theme.colors.yellow};
  }
`

export const SlideSubMenuContent = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem;
  transition: all .5s ease;
`
