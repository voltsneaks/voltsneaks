import Link from 'next/link'
import React, { useState } from 'react'
import { AiFillCaretLeft } from 'react-icons/ai'
import { SlideMenuItem, MobileAnchorWrapper, SubMenuAnchor, SlideSubMenuContent } from './SubMenu.elements'

const SubMenu = ({ category, info, children, setClick }) => {
  const [open, setOpen] = useState(false)

  const closeMenu = () => {
    return setOpen(false), setClick(false)
  }

  return (
     <SlideMenuItem open={open}>
       <MobileAnchorWrapper onClick={() => setOpen(!open)}><SubMenuAnchor>{children}</SubMenuAnchor><AiFillCaretLeft /></MobileAnchorWrapper>
       {open
         ? (
           <SlideSubMenuContent>
           { info.map((items) => (
           <Link href={`/categorias/${category}/${items.value}`}>
            <SubMenuAnchor onClick={() => closeMenu()}submenu>{items.value}</SubMenuAnchor>
           </Link>
           )) }
           </SlideSubMenuContent>
           )
         : null
       }
     </SlideMenuItem>
  )
}

export default SubMenu
