import { LogAnchor, Text } from 'components/atoms'
import { useState } from 'react'
import { VscAccount } from 'react-icons/vsc'
import { AccountMenuWrapper, AccountDropMenu } from './AccountMenu.elements'

const AccountMenu = () => {
  const [open, setOpen] = useState(false)

  return (
        <AccountMenuWrapper>
            <VscAccount size="2.5rem" onClick={() => setOpen(!open)} color={open ? '#ff6600' : '#292929'}/>
            <AccountDropMenu open={open}>
                <LogAnchor login margin="0 1.6rem 0 0" href="/login" onClick={() => setOpen(false)}>Acceder</LogAnchor>
                <LogAnchor href="/registration" onClick={() => setOpen(false)}>Registrarse</LogAnchor>
            </AccountDropMenu>
        </AccountMenuWrapper>
  )
}

export default AccountMenu
