import styled from 'styled-components'

export const AccountMenuWrapper = styled.div`
  position: relative;
  cursor: pointer;
`

export const AccountDropMenu = styled.div`
  position: absolute;
  display: flex;
  width: 1.5rem;
  top: 0;
  right: 23.5rem;
  opacity: ${({ open }) => open ? '1' : '0'};
  transform: ${({ open }) => open ? 'translateY(0%)' : 'translateY(-60%)'};
  transition: opacity 500ms ease-in-out, transform 500ms ease-in-out;
`
