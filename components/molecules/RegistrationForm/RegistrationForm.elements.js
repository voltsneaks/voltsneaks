import styled from 'styled-components'

export const RegistrationWrapper = styled.form`
  margin: 14rem auto;
  width: 40rem;
  height: 35rem;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  align-items: center;
`

export const SuscribeWrapper = styled.div`
  width: 100%;
  margin-bottom: 2rem;
  display: flex;
  justify-content: flex-start;
  align-items: center;

    input{
      margin: 0;
      width: 3rem;
      margin-right: 2rem;
    }
    label{
      color: ${({ theme }) => theme.colors.lightGrey2};
      font-size: 1.2rem;
    }
`

export const Input = styled.input`
  width: 100%;
  padding: .8rem;
  margin-bottom: 1.5rem;
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
  border-radius: 2px;
  font-size: 1.6rem;
`
