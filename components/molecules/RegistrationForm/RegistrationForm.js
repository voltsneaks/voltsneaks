// import { RegistrationWrapper, SuscribeWrapper, Input } from "./RegistrationForm.elements"
// import { Anchor, ButtonSubmit, Text } from "components/atoms"
// import { useAuth } from "contexts/AuthProvider"
// import { SignupSchema } from "services/validationSchema"
// import { useForm } from 'react-hook-form'
// import { yupResolver } from '@hookform/resolvers/yup'
// import { useState } from "react"

// const RegistrationForm = () => {

// const [error, setError] = useState(false)
// const { signup, authError, currentUser } = useAuth()

// const { register, handleSubmit, errors } = useForm({
//     resolver: yupResolver(SignupSchema)
// })

// const onSubmit = (values) => {
//     const {nombre, email, password} = values
//     signup(nombre, email, password)
// }

//     return (
//         <RegistrationWrapper onSubmit={handleSubmit(onSubmit)}>
//             {currentUser && <Text color="red" size="xSmall">{currentUser.email}</Text>}
//             <Text size="xLarge" margin="0 0 3rem 0" weight="bold" spacing="-.15rem">CREAR NUEVA CUENTA</Text>
//             <Input type="text" placeholder="Nombre"
//             name="nombre"
//             ref={register}
//             error={errors.nombre}
//             />
//             {errors.nombre && <Text color="red" size="xSmall">{errors.nombre.message}</Text>}
//             <Input type="text" placeholder="Apellido"
//             name="apellido"
//             ref={register}
//             error={errors.apellido}
//             />
//             {errors.apellido && <Text color="red" size="xSmall">{errors.apellido.message}</Text>}
//             <Input type="text" placeholder="Rut"
//             name="rut"
//             ref={register}
//             error={errors.rut}
//             />
//             {errors.rut && <Text color="red" size="xSmall">{errors.rut.message}</Text>}
//             <Input type="email" placeholder="Email"
//             name="email"
// ref={register}
// error={errors.email}
//             />
//             {errors.email && <Text color="red" size="xSmall">{errors.email.message}</Text>}
//             <Input type="password" placeholder="Contraseña"
//             name="password"
//             ref={register}
//             error={errors.password}
//             />
//             {errors.password && <Text color="red" size="xSmall">{errors.password.message}</Text>}
//             <SuscribeWrapper>
//             <Input type="checkbox" />
//             <label>Regístrate para recibir actualizaciones por correo electrónico de Voltsneaks sobre productos, ofertas y beneficios</label>
//             </SuscribeWrapper>
//             {authError && <Text color="red" size="xSmall">{authError}</Text>}
//             <ButtonSubmit width="100%" padding="1rem">Crear Cuenta</ButtonSubmit>
//             <Text margin="1.5rem 0 0 0" size="small">¿Ya tienes una cuenta?</Text><Anchor bold size="small" decoration="underline" href="/login">Inicia Sesion</Anchor>
//         </RegistrationWrapper>
//     )
// }

// export default RegistrationForm
