import styled from 'styled-components'

export const NavMenuWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  list-style: none;
`

export const NavMenuSection = styled.div`//drop-down
  height: 100%;
  list-style: none;
  display: block;

  :hover{
    div{
      opacity: 1;
      visibility: visible;
    }
  }
`

export const NavMenuAnchor = styled.a`
  color: ${({ theme }) => theme.colors.fontPrimary};
  margin: 0 2rem 0 1rem;
  font-weight: 600;
  font-size: 1.5rem;
  cursor: pointer; 
  transition: color 150ms ease-in;

  :hover{
    color: ${({ theme }) => theme.colors.orange};
  }
`

export const NavMenuDropDown = styled.div`//menu-area
  list-style: none;
  position:absolute;
  display: flex;
  flex-wrap: wrap;
  align-items: flex-end;
  justify-content: center;
  width: 100%;
  height: 4rem;
  top: 60%;
  left: 0;
  transition: all .2s ease ;
  opacity: 0; 
  visibility: hidden;
  background-color: white;
`

export const DropDownItem = styled.a`
  color: ${({ theme }) => theme.colors.fontPrimary};
  margin: 0 2rem 0 1rem;
  font-size: 1.3rem;
  cursor: pointer; 
  padding-bottom: 1rem;
  font-weight: 600;
  transition: color 150ms ease-in;

  :hover{
    color: ${({ theme }) => theme.colors.orange};
  }
`
