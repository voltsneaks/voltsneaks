import { NavMenuAnchor, NavMenuWrapper, NavMenuSection, NavMenuDropDown, DropDownItem } from './NavMenu.elements'
import { marcas, calzado, accesorios } from 'services/MenuItems'
import Link from 'next/link'

const NavMenu = () => {
  return (

        <NavMenuWrapper>
          <NavMenuSection>
            <Link href="/">
            <NavMenuAnchor>Home</NavMenuAnchor>
            </Link>
          </NavMenuSection>
          <NavMenuSection>
            <NavMenuAnchor>Marcas</NavMenuAnchor>
            <NavMenuDropDown>
            {
              marcas.map((info) => (
                <Link href={`/categorias/marcas/${info.value}`}>
                <DropDownItem key={info.id}>{info.value}</DropDownItem>
                </Link>
              ))
            }
            </NavMenuDropDown>
          </NavMenuSection>
          <NavMenuSection>
            <NavMenuAnchor>Calzado</NavMenuAnchor>
            <NavMenuDropDown>
            {
              calzado.map((info) => (
                <Link href={`/categorias/calzado/${info.value}`}>
                <DropDownItem key={info.id}>{info.value}</DropDownItem>
                </Link>
              ))
            }
            </NavMenuDropDown>
          </NavMenuSection>
          <NavMenuSection>
            <NavMenuAnchor>Accesorios</NavMenuAnchor>
            <NavMenuDropDown>
            {
              accesorios.map((info) => (
                <Link href={`/categorias/accesorios/${info.value}`}>
                <DropDownItem key={info.id}>{info.value}</DropDownItem>
                </Link>
              ))
            }
            </NavMenuDropDown>
          </NavMenuSection>
          {/* <NavMenuSection>
            <Link href="/nuevo">
            <NavMenuAnchor>Nuevo</NavMenuAnchor>
            </Link>
          </NavMenuSection> */}
        </NavMenuWrapper>
  )
}
export default NavMenu
