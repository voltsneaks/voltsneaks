import styled from 'styled-components'
import theme from 'styles/theme'

export const ProductGalleryWrapper = styled.div`
  width: 45%;

  @media ${theme.breakpoints.mobile}{
    width: 95%;
    margin: 0 auto 2rem auto;
  }
  `

export const ThumbnailWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

export const Thumbnail = styled.img`
  width: 22.5%;
  margin: 2rem 0;
  opacity: 0.5;
  cursor: pointer;
  transition: opacity 200ms ease-in;

  :hover{
      opacity: 1;
  }

  @media ${theme.breakpoints.mobile}{
    margin: 1rem 0;
  }
`
