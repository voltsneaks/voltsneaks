import { Image, Thumbnail } from 'components/atoms'
import { useState } from 'react'
import { ProductGalleryWrapper, ThumbnailWrapper } from './ProductGallery.elements'

const ProductGallery = ({ imagenes }) => {
  const [imagen, setImagen] = useState(imagenes[0].formats.medium.url)

  return (
        <ProductGalleryWrapper>
            <Image src={imagen}/>
            <ThumbnailWrapper>
                <Thumbnail src={imagenes[1].formats.small.url}
                onClick={() => setImagen(imagenes[1].formats.medium.url)}
                />
                <Thumbnail src={imagenes[2].formats.small.url}
                onClick={() => setImagen(imagenes[2].formats.medium.url)}
                />
                <Thumbnail src={imagenes[3].formats.small.url}
                onClick={() => setImagen(imagenes[3].formats.medium.url)}
                />
                <Thumbnail src={imagenes[0].formats.small.url}
                onClick={() => setImagen(imagenes[0].formats.medium.url)}
                />
            </ThumbnailWrapper>
        </ProductGalleryWrapper>
  )
}

export default ProductGallery
