import { Anchor, LogAnchor } from 'components/atoms'
import styled from 'styled-components'
import theme from 'styles/theme'
import { MdClear } from 'react-icons/md'

export const MobileMenuWrapper = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(-100%)'};
  transition: transform 200ms ease-in-out;
  list-style: none;
  background-color:white;
  width: 50%;
  height: 100vh;
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;
  animation: fadein 8s;
  z-index: 9999;

@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

  @media ${({ theme }) => theme.breakpoints.mobile}{
    width: 100%;
  }
`

export const MobileAnchor = styled(Anchor)`
  color: ${({ theme }) => theme.colors.fontPrimary};
  font-size: ${({ submenu }) => submenu ? '2rem' : '2.5rem'};
  font-weight: ${({ submenu }) => submenu ? '400' : '600'};
  padding: 2rem 0;
`

export const MobileAnchorWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 1.2rem 0;
`

export const SlideMenu = styled.div`
  padding: 5rem 5rem 18rem 5rem;
  display: flex;
  flex-direction: column;
  height: 70rem;
`

export const SlideMenuLogAnchor = styled(LogAnchor)`
  width: 100%;
  padding: 1rem;
  border-radius: 3px;
  border: 2px solid white;
  font-size: 2.5rem;
  font-weight: 600;
  text-align: center;
  margin-top: 4rem;
  color: ${({ login }) => login ? theme.colors.orange : 'white'};
  background-color: ${({ login }) => login ? 'white' : theme.colors.orange};


    :hover{
      background-color: ${({ theme }) => theme.colors.fontSecundary};
    }
`

export const MobileControlsWrapper = styled.div`
  width: 90%;
  padding: 1rem;
  margin: 2rem auto;
`

export const MobileCloseIcon = styled(MdClear).attrs({ size: '2.5rem' })`
  color: ${({ theme }) => theme.colors.fontPrimary};
  cursor: pointer;
`
