import React from 'react'
import { IconContext } from 'react-icons'
import { MobileAnchor, MobileAnchorWrapper, MobileCloseIcon, MobileControlsWrapper, MobileMenuWrapper, SlideMenu } from './MobileMenu.elements'
import { marcas, accesorios, calzado } from 'services/MenuItems'
import { SubMenu } from '..'

const MobileMenu = ({ click, setClick }) => {
  return (
      <IconContext.Provider value={{ color: '#292929', className: 'icons', size: '1.7rem' }}>

      <MobileMenuWrapper open={click}>
        <MobileControlsWrapper onClick={() => setClick(!click) }>
          <MobileCloseIcon/>
        </MobileControlsWrapper>
        <SlideMenu >
            <MobileAnchorWrapper>
              <MobileAnchor href="/" onClick={() => setClick(false)}>Home</MobileAnchor>
              </MobileAnchorWrapper>
          <SubMenu category="marcas" setClick={setClick} info={marcas}>Marcas</SubMenu>
          <SubMenu category="calzado" setClick={setClick} info={calzado}>Calzado</SubMenu>
          <SubMenu category="accesorios" setClick={setClick} info={accesorios}>Accesorios</SubMenu>
          {/* <MobileAnchorWrapper>
            <MobileAnchor href="/novedades" onClick={() => setClick(false)}>Novedades</MobileAnchor>
            </MobileAnchorWrapper> */}
          {/* <SlideMenuLogAnchor onClick={() => setClick(false)} login href="/login">Acceder</SlideMenuLogAnchor> */}
          {/* <SlideMenuLogAnchor onClick={() => setClick(false)} href="/registration">Registrarse</SlideMenuLogAnchor> */}
        </SlideMenu>
      </MobileMenuWrapper>

      </IconContext.Provider>
  )
}

export default MobileMenu
