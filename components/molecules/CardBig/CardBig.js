import { Image, Text } from 'components/atoms'
import Link from 'next/link'
import { CardBigAnchor, CardBigInfo, CardBigWrapper } from './CardBig.elements'

const CardBig = ({ title, subTitle, width, href, ...rest }) => {
  return (
      <CardBigWrapper width={width}>
      <Link href={href}>
      <Image pointer cover height="100%" {...rest}/>
      </Link>
        <CardBigInfo>
         <Text color="white" margin="0 0 1rem 0" weight="bold" size="large">{title}</Text>
         <Text color="white" >{subTitle}</Text>
         <Link href={href}>
           <CardBigAnchor>Ver Mas</CardBigAnchor>
         </Link>
        </CardBigInfo>

    </CardBigWrapper>
  )
}

export default CardBig
