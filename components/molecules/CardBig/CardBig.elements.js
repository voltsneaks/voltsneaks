import styled from 'styled-components'

export const CardBigWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-bottom: 3rem;
  width: ${({ width }) => width || '100%'};
  
  @media ${({ theme }) => theme.breakpoints.mobile}{
    width: 100%;
  }
`
export const CardBigBody = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  `

export const CardBigInfo = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  padding: 1.5rem;
  margin: 2% 2%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  min-height: 5rem;
  background-color: rgba(0, 0, 0, 0.548);
`

export const CardBigAnchor = styled.a`
  color: white;
  font-size: 1.6rem;
  font-weight: 500;
  cursor: pointer;
  margin-top: 1rem;
  letter-spacing: -.5px;
  transition: color 200ms ease-in;

    &:hover{
      color: ${({ theme }) => theme.colors.orange};
      text-decoration: underline;
    }
`
