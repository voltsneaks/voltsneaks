import styled from 'styled-components'

export const CardWrapper = styled.div`
  position: relative;

  display: flex;
  align-items: left;
  flex-wrap: wrap;
  flex-direction: column;
  
  box-sizing: border-box;
  flex: 1 1 25%;
  padding: 1.6rem;
  border: .1rem solid transparent;
  border-radius: .2rem;
  transition: border 200ms ease-in, box-shadow 200ms ease-in;

  &:hover{
      border: .1rem solid ${({ theme }) => theme.colors.orange};
      box-shadow: .6rem .6rem ${({ theme }) => theme.colors.orange};      
    }
`
export const BodyWrapper = styled.div`
  display: flex;
  position: relative;
  align-items: flex-end;

  svg {
    cursor: pointer;
  }
`

export const CardBody = styled.div`
  width: 100%;
  max-height: 6rem;
  margin-top: 2.6rem;
  background-color: white;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  flex: 1 1 auto;
  box-sizing: border-box;

  p {
    margin: .2rem 0;
  }
`
export const Span = styled.span`
  position: absolute;
  top: 2.4rem;
  left: 2.4rem;
  color:white;
  background-color:${({ theme }) => theme.colors.fontPrimary};
  padding: 1rem;
  z-index: 10;
  border-radius: .2rem;
  font-size: 1.2rem;
  font-weight: bold;
`
