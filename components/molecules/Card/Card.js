import { Image, Text } from 'components/atoms'
import Link from 'next/link'
import { CardWrapper, BodyWrapper, CardBody, Span } from './Card.elements'

const Card = ({ ...products }) => {
  return (
    <>
      <CardWrapper key={products.id}>
        {!products.disponible && <Span>Agotado</Span>}
        <Link href={`/details/${products.id}`}>
         <Image pointer cover src={products.imagenes[0].formats.small.url} />
        </Link>
        <BodyWrapper>
          <CardBody>
            <Text>{products.marca.nombre}</Text>
            <Text weight="bold">{products.modelo}</Text>
            <Text>${products.precio}</Text>
          </CardBody>
        </BodyWrapper>
      </CardWrapper>
    </>
  )
}

export default Card
