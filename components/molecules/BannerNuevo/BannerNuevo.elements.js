import styled from 'styled-components'
import theme from 'styles/theme'

export const BannerNuevoWrapper = styled.div`
  width: 100%;
  background-color: ${({ theme }) => theme.colors.orange};
  height: 6rem;
  display: flex;
  justify-content: flex-start;
  margin-bottom: 5rem;

  @media ${theme.breakpoints.mobile}{
  height: 4vh;
  margin-bottom: 2rem;
  }
`

export const BannerNuevoTextTittle = styled.p`
  display: block;
  font-size: 6rem;
  line-height: 6rem;
  letter-spacing: -.2rem;
  color: white;
  font-family: ${({ theme }) => theme.fonts.logo};
  margin-left: 13rem;

  @media ${theme.breakpoints.mobile}{
    font-size: 4rem;
    line-height: 4rem;
    letter-spacing: -.1rem;
    margin-left: 2.4rem;
  }
`
