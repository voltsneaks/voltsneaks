import { BannerNuevoWrapper, BannerNuevoTextTittle } from './BannerNuevo.elements'

const BannerNuevo = () => {
  return (
        <BannerNuevoWrapper>
          <BannerNuevoTextTittle>NUEVO</BannerNuevoTextTittle>
        </BannerNuevoWrapper>
  )
}

export default BannerNuevo
