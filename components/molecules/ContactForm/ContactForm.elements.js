import styled from 'styled-components'

export const ContactFormWrapper = styled.form`
  margin: 8rem auto;
  width: 50rem;
  height: 40rem;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  align-items: center;

  textarea{
    width: 100%;
    height: 14rem;
    margin-bottom: 1.5rem;
    padding: .8rem;
    font-size: 1.6rem;

    ::placeholder{
      font-size: 1.6rem;
    }
  }

  @media ${({ theme }) => theme.breakpoints.mobile}{
    width: 90%;
  }
`
