import { ContactFormWrapper } from './ContactForm.elements'
import { ButtonSubmit, Input, Text } from 'components/atoms'
import Link from 'next/link'

const ContactForm = () => {
  return (
        <ContactFormWrapper >
            <Text size="xLarge" margin="0 0 3rem 0" weight="bold" spacing="-.15rem">ESCRIBENOS</Text>
            <Input type="text" placeholder="Nombre"/>
            <Input type="email" placeholder="Email"/>
            <Input type="text" placeholder="Numero telefonico"/>
            <textarea placeholder="Deja tu comentario..."/>
            <Link href="/">
            <ButtonSubmit width="100%" padding="1rem">Enviar</ButtonSubmit>
            </Link>
        </ContactFormWrapper>
  )
}

export default ContactForm
