import styled from 'styled-components'
import theme from 'styles/theme'

export const BannerWrapper = styled.div`
  width: 100%;
  background-color: ${({ bgColor }) => bgColor};
  height: 18vh;
  display: flex;
  overflow: hidden;
  margin: ${({ margin }) => margin};

  @media ${theme.breakpoints.tablet}{
  height: 11vh;
  }
  @media ${theme.breakpoints.mobile}{
  height: 10vh;
  }
  @media ${theme.breakpoints.mobile2}{
  height: 8vh;
  }

`

export const BannerTextTittle = styled.p`
  display: block;
  font-size: 19.5rem;
  letter-spacing: -2rem;
  color: ${({ fColor }) => fColor || theme.colors.fontPrimary};

  @media ${theme.breakpoints.tablet}{
    font-size: 15rem;
    letter-spacing: -1.5rem;
  }
  @media ${theme.breakpoints.mobile}{
    font-size: 10.5rem;
    line-height: 17.5rem;
    letter-spacing: -1rem;
  }
  @media ${theme.breakpoints.mobile2}{
    font-size: 9rem;
    line-height: 16rem;
  }
`
