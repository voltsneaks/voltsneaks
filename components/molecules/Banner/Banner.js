import { BannerWrapper, BannerTextTittle } from './Banner.elements'

const Banner = ({ children, font, fColor, ...rest }) => {
  return (
        <BannerWrapper {...rest}>
          <BannerTextTittle fColor={fColor}>{children}</BannerTextTittle>
        </BannerWrapper>
  )
}

export default Banner
