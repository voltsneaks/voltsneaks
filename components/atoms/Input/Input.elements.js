import styled from 'styled-components'

export const InputWrapper = styled.input`
  width: 100%;
  padding: .8rem;
  margin-bottom: 1.5rem;
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
  border-radius: 2px;
  font-size: 1.6rem;
`
