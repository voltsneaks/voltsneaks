import { InputWrapper } from './Input.elements'

const Input = (props) => {
  return (
        <InputWrapper {...props}
        type={props.type}
        id={props.id}
        ref={props.ref}
        />
  )
}

export default Input
