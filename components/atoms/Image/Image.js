import { StyledImage } from './Image.elements'
import 'react-lazy-load-image-component/src/effects/blur.css'

const Image = ({ scrollPosition, ...rest }) => {
  return (
      <StyledImage
      {...rest}
      scrollPosition={scrollPosition}
      effect="blur"
      placeholderSrc="./placeholder.jpg"
      />
  )
}

export default Image
