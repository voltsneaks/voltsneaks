import styled from 'styled-components'
import { LazyLoadImage } from 'react-lazy-load-image-component'

export const StyledImage = styled(LazyLoadImage)`
  width: ${props => props.width || '100%'};
  height: ${props => props.width || 'null'};
  cursor: ${({ pointer }) => (pointer ? 'pointer' : null)};
  object-fit: ${({ cover }) => (cover ? 'cover' : null)};
  filter: brightness(100%);
  transition: filter 500ms ease-in-out;
  display: block;
`
