import { BsBag } from 'react-icons/bs'
import { CartIconWrapper, Span } from './CartIcon.elements'

const CartIcon = ({ info }) => {
  return (
        <CartIconWrapper>
            {info > 0 && <Span>{info}</Span>}
            <BsBag size="2.5rem"/>
        </CartIconWrapper>
  )
}

export default CartIcon
