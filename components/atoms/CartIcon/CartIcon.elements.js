import styled from 'styled-components'

export const CartIconWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 3.7rem;
  height: 4rem;
  cursor: pointer;
`
export const Span = styled.span`
  position: absolute;
  right: .25rem;
  top: .6rem;
  color: white;
  background-color: ${({ theme }) => theme.colors.orange};
  border-radius: 1rem;
  width: 1.7rem;
  height: 1.7rem;
  padding-left: .68rem;
  padding-top: .25rem;
  font-size: 1rem;
`
