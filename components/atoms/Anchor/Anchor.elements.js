import { StyledText } from 'components/atoms/Text/Text.elements'
import styled from 'styled-components'

export const StyledAnchor = styled(StyledText)`
    cursor: pointer;
    font-size: ${props => {
        switch (props.size) {
            case 'xLarge':
                return '2.4rem'
            case 'large':
                return '2rem'
            case 'small':
                return '1.3rem'
            case 'xSmall':
                return '1rem'
            case 'xxSmall':
                return '0.7rem'
            default:
                return '1.6rem'
        }
    }};
    text-decoration: ${props => {
        switch (props.decoration) {
            case 'underline':
                return 'underline'
            case 'line-through':
                return 'line-through'
            default:
                return 'none'
        }
    }};
    text-transform: ${props => props.transform || 'none'};
    color: ${props => {
        switch (props.color) {
            case 'grey':
                return props.theme.colors.fontSecundary
            case 'orange':
                return props.theme.colors.orange
            case 'white':
                return 'white'
            default:
                return props.theme.colors.fontPrimary
        }
    }};
    font-weight: ${({ bold }) => bold ? '600' : '400'};
    margin-left: ${({ marginLeft }) => marginLeft || 'none'};
`
