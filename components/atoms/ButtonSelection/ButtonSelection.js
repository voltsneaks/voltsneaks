import React, { useState } from 'react'
import { ButtonSelectionWrapper } from './ButtonSelection.elements'

const ButtonSelection = (props) => {
  const [activeNumber, setActiveNumber] = useState('')

  const getNumber = (num) => {
    if (!activeNumber) {
      setActiveNumber(num)
      console.log(activeNumber)
    } else {
      null
    }
  }

  return (
        <ButtonSelectionWrapper {...props} onClick={() => getNumber(props.number)} activeNumber={activeNumber}>
            {props.children}
        </ButtonSelectionWrapper>
  )
}

export default ButtonSelection
