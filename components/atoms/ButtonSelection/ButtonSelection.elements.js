import styled from 'styled-components'
import theme from 'styles/theme'

export const ButtonSelectionWrapper = styled.a`
  color: ${({ activeNumber }) => activeNumber ? 'white' : theme.colors.fontPrimary};
  border-color: ${({ theme }) => theme.colors.fontPrimary};
  padding: ${({ padding }) => padding || '.3rem 1rem'};
  width: ${({ width }) => width || null};
  margin: ${({ margin }) => margin || null};
  flex: ${({ flex }) => flex || null};
  text-align: center;
  font-size: 1.6rem;
  font-weight: 500;  
  border: 1px solid ${({ theme }) => theme.colors.fontPrimary};
  background-color: ${({ activeNumber }) => (activeNumber ? theme.colors.fontSecundary : 'white')};
  border-radius: .2rem;
  cursor: pointer;
  letter-spacing: -.5px;
  transition: all 200ms ease-in;

    :hover{
        background-color: ${({ login }) => (login ? theme.colors.orange : theme.colors.fontSecundary)};
        border-color: transparent;
        color: white;
    }

    :-ms-input-placeholder{
        color: red;
    }
`
