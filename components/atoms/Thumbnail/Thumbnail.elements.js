import styled from 'styled-components'

export const ThumbnailWrapper = styled.img`
  width: 22.5%;
  margin: 2rem 0;
  opacity: ${({ selected }) => selected ? '1' : '0.5'};
  cursor: pointer;
  transition: opacity 200ms ease-in;

  :hover{
      opacity: 1;
  }
`
