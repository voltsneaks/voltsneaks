import { ThumbnailWrapper } from './Thumbnail.elements'

const Thumbnail = (props) => {
  return (
        <ThumbnailWrapper {...props} />
  )
}

export default Thumbnail
