import styled from 'styled-components'
import theme from 'styles/theme'

export const SubmitWrapper = styled.button`
  color: ${({ theme }) => theme.colors.fontPrimary};
  border-color: ${({ theme }) => theme.colors.fontPrimary};
  padding: ${({ padding }) => padding || '.3rem 1rem'};
  width: ${({ width }) => width || null};
  margin: ${({ margin }) => margin || null};
  text-align: center;
  font-size: 1.6rem;
  font-weight: 500;  
  border: 1px solid ${({ theme }) => theme.colors.fontPrimary};
  background-color: white;
  border-radius: .2rem;
  cursor: pointer;
  letter-spacing: -.5px;
  transition: background-color 200ms ease-in, border 200ms ease-out, color 200ms ease-in;

    &:hover{
        background-color: ${({ login }) => (login ? theme.colors.orange : theme.colors.fontSecundary)};
        border-color: transparent;
        color: white;
    }

`
