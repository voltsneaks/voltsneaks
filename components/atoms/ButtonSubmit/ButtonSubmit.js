import { SubmitWrapper } from './ButtonSubmit.elements'

const ButtonSubmit = (props) => {
  return (
      <SubmitWrapper {...props}>
        { props.children }
      </SubmitWrapper>
  )
}

export default ButtonSubmit
