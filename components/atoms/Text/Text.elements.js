import styled from 'styled-components'
import theme from 'styles/theme'

export const StyledText = styled.p`
    text-align: ${props => {
        switch (props.align) {
            case 'center':
                return 'center'
            case 'right':
                return 'right'
            default:
                return 'left'
        }
    }};
    font-size: ${props => {
        switch (props.size) {
            case 'xxLarge':
                return '3rem'
            case 'xLarge':
                return '2.4rem'
            case 'large':
                return '2rem'
            case 'medium':
                return '1.6rem'
            case 'small':
                return '1.4rem'
            case 'xSmall':
                return '.7rem'
            case 'xxSmall':
                return '0.5rem'
            default:
                return '1.4rem'
        }
    }};
    margin: ${({ margin }) => margin || 'none'};
    font-weight: ${props => {
        switch (props.weight) {
            case 'thin':
                return '200'
            case 'normal':
                return '400'
            case 'bold':
                return '600'
            default:
                return '400'
        }
    }};
    text-decoration: ${props => {
        switch (props.decoration) {
            case 'underline':
                return 'underline'
            case 'line-through':
                return 'line-through'
            default:
                return 'none'
        }
    }};
    text-transform: ${props => props.transform || 'none'};
    line-height: ${props => {
            switch (props.size) {
                case 'xxLarge':
                return '3rem'
            case 'xLarge':
                return '2.4rem'
            case 'large':
                return '2rem'
            case 'medium':
                return '1.6rem'
            case 'small':
                return '1.4rem'
            case 'xSmall':
                return '.7rem'
            case 'xxSmall':
                return '0.5rem'
            default:
                return '1.4rem'
            }
        }};
    align-self: ${({ alignSelf }) => alignSelf || 'none'};
    color: ${({ color }) => color || theme.colors.fontPrimary};
    cursor: ${({ pointer }) => (pointer ? 'pointer' : null)};
    letter-spacing: ${({ spacing }) => spacing || null};
`
