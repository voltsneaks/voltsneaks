import Link from 'next/link'
import { StyledAnchor } from './LogAnchor.elements'

const LogAnchor = (props) => {
  return (
        <Link href={props.href} passHref>
            <StyledAnchor {...props}>{props.children}</StyledAnchor>
        </Link>
  )
}

export default LogAnchor
