import Link from 'next/link'
import { LogoType, LogoWrapper } from './Logo.elements'

const Logo = () => {
  return (
        <Link href="/" passHref>
            <LogoType>
                VOLT<span>SNEAKS</span>
            </LogoType>
        </Link>
  )
}

export default Logo
