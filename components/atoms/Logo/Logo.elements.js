import styled from 'styled-components'

export const LogoWrapper = styled.div`
  width: 150px;
`

export const LogoType = styled.p`
  color: ${({ theme }) => theme.colors.orange};
  font-size: 2.2rem;
  letter-spacing: -1px;
  line-height: 0;
  font-family: ${({ theme }) => theme.fonts.logo};
  cursor: pointer;

    span {
      color: ${({ theme }) => theme.colors.fontPrimary};
      letter-spacing: -1px;

    }
`
