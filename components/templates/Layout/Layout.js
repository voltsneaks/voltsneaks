import { Nav } from 'components/organisms'
import Footer from 'components/organisms/Footer/Footer'
import { useData } from 'contexts/DataProvider'
import Head from 'next/head'
import { Container } from './Layout.elements'

const Layout = ({ children }) => {
  const { carrito } = useData()

  return (
        <>
      <Head>
        <title>VoltSneaks</title>
      </Head>
      <Nav carrito={carrito}/>
            {children}
        <Footer></Footer>
        </>
  )
}

export default Layout
