import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  min-height: ${({ minHeight }) => minHeight || '80rem'};
  max-width: 1440px;
  padding: 0 5rem;
  margin: 0 auto;
  display: flex;
  flex-flow: column nowrap;

@media ${props => props.theme.breakpoints.tablet}{
  padding: 0 1rem;
  }
`
