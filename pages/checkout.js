import { CheckoutForm } from 'components/molecules'
import { BolsaOverview } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'
import { useData } from 'contexts/DataProvider'
import { useEffect } from 'react'
import styled from 'styled-components'
import Router from 'next/router'

const CheckoutWrapper = styled.div`
  width: 100%;
  margin: 2rem auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;


  @media ${({ theme }) => theme.breakpoints.mobile}{
    flex-direction: column-reverse;  
  } 
`

const FormSectionWrapper = styled.div`
  width: 55%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto 5rem auto;

  @media ${({ theme }) => theme.breakpoints.mobile}{
    width: 95%;
  } 
`
const BolsaOverviewWrapper = styled.div`
  width: 35%;

  @media ${({ theme }) => theme.breakpoints.mobile}{
    width: 100%;  
  } 
`

const checkout = () => {
  const { carrito } = useData()

  useEffect(() => {
    if (carrito.length === 0) {
      Router.push('/empty')
    }
  }, [carrito])

  return (
    <Container>
      <CheckoutWrapper>
          <FormSectionWrapper>
            <CheckoutForm/>
          </FormSectionWrapper>

          <BolsaOverviewWrapper>
            <BolsaOverview/>
          </BolsaOverviewWrapper>
      </CheckoutWrapper>
    </Container>
  )
}

export default checkout
