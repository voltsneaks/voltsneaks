import { DataProvider } from 'contexts/DataProvider'
import theme from 'styles/theme'
import { ThemeProvider } from 'styled-components'
import { Global } from 'styles/GlobalStyles'
import { Layout } from 'components/templates'

function MyApp ({ Component, pageProps }) {
  return (

    <DataProvider>
        <ThemeProvider theme={theme}>
          <Layout>
          <Global/>
          <Component {...pageProps} />
          </Layout>
        </ThemeProvider>
    </DataProvider>
  )
}

export default MyApp
