import { Text } from 'components/atoms'
import { Container } from 'components/templates/Layout/Layout.elements'
import styled from 'styled-components'

const EmptyWrapper = styled.div`
  width: 70%;
  margin: 25% auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const checkout = () => {
  return (
    <Container>
      <EmptyWrapper>
        <Text align="center" margin="0 0 1rem 0" size="xxLarge">LA BOLSA ESTA VACIA</Text>
        <Text align="center" size="large">Aun no haz añadido ningun producto a tu bolsa de compras</Text>
      </EmptyWrapper>
    </Container>
  )
}

export default checkout
