import { Card, Hero, Banner } from 'components/molecules'
import CardBig from 'components/molecules/CardBig/CardBig'
import { Grid } from 'components/organisms/Productos/Productos.elements'
import { Container } from 'components/templates/Layout/Layout.elements'
import styled from 'styled-components'

const PromoWrapper = styled.div`
  width: 100%;
  margin: 5rem auto 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0 1rem;

  @media ${({ theme }) => theme.breakpoints.mobile}{
    flex-direction: column;
  }
`

function home ({ product }) {
  const running = product.filter((prod) => prod.categorias[0].nombre === 'Running')
  const runningAdidas = running.filter((prod) => prod.marca.nombre === 'Adidas')

  return (
      <>
        <Hero />
        <Container minHeight="0">
          <PromoWrapper>
            <CardBig
            href="/categorias/marcas/Diadora"
            src="./diadorademo.jpg"
            title="Diadora"
            subTitle="Nuevos modelos de la popular marca italiana."
            width="48%"
            />
            <CardBig
            href="/details/5fbfd3761977a21bd74959ff"
            src="./adidas4d.png"
            title="Adidas Performance 4D Run 1.2"
            subTitle="Descubre lo ultimo en tecnologia para corredores."
            width="48%"
            />
          </PromoWrapper>
        </Container>
        <Banner bgColor={'rgb(255, 255, 0)'} margin="8rem 0 1rem 0">{'RUNNING >>'}</Banner>
        <Container minHeight="0">
          <Grid>
          {
            runningAdidas.map((prod) => (
              <Card {...prod}/>
            ))
          }
          </Grid>
        </Container>
      </>
  )
}

export async function getStaticProps () {
  const data = await fetch('https://voltsneaks-api.herokuapp.com/products?_limit=-1')
  const product = await data.json()
  return {
    props: { product },
    revalidate: 1
  }
}

export default home
