import { Text } from 'components/atoms'
import ContactForm from 'components/molecules/ContactForm/ContactForm'
import { Container } from 'components/templates/Layout/Layout.elements'
import styled from 'styled-components'

export const ContactWrapper = styled.div`
  width: 100%;
  margin: 4rem auto;
`

export const TextBody = styled.div`
  width: 65%;
  margin: 4rem auto;
  word-wrap: break-word;
`
export const Title = styled.p`
  text-align: ${({ align }) => align || 'left'};
  font-size: 3rem;
  font-weight: bold;
  margin-bottom: 5rem;
  `
export const Subtitle = styled.p`
  text-align: ${({ align }) => align || 'left'};
  font-size: ${({ big }) => big ? '1.8rem' : '1.6rem'};
  font-weight: bold;
  margin: 3rem 0;
`

const contacto = () => {
  return (
    <Container>
      <ContactWrapper>
        <TextBody>
          <Subtitle big align="center">Contáctate con nosotros</Subtitle>
          <Text align="center" >VOLTSNEAKS es la tienda donde estan las mejores marcas y calzado para la práctica del deporte profesional y recreativo. Nos gustaría responderle todas sus consultas, comuníquese con nosotros estamos para ayudarlo. </Text>
        </TextBody>
        <ContactForm />
      </ContactWrapper>
    </Container>
  )
}

export default contacto
