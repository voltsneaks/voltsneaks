import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Casual = () => {
  return (
    <Container>
      <Productos category="calzado" index="Casual"/>
    </Container>
  )
}

export default Casual
