import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Running = () => {
  return (
    <Container>
      <Productos category="calzado" index="Running"/>
    </Container>
  )
}

export default Running
