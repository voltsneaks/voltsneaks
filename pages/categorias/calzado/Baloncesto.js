import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Baloncesto = () => {
  return (
    <Container>
      <Productos category="calzado" index="Basquetball"/>
    </Container>
  )
}

export default Baloncesto
