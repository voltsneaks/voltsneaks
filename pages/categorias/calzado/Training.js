import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Training = () => {
  return (
    <Container>
      <Productos category="training" index="Training"/>
    </Container>
  )
}

export default Training
