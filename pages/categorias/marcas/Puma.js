import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Puma = () => {
  return (
      <Container>
        <Productos category="marcas" index="Puma"/>
      </Container>
  )
}

export default Puma
