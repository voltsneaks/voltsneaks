import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const NewBalance = () => {
  return (
      <Container>
          <Productos category="marcas" index="New Balance"/>
      </Container>
  )
}

export default NewBalance
