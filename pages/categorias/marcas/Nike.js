import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Nike = () => {
  return (
      <Container>
          <Productos category="marcas" index="Nike"/>
      </Container>
  )
}

export default Nike
