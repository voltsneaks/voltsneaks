import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Adidas = () => {
  return (
      <Container>
          <Productos category="marcas" index="Adidas"/>
      </Container>
  )
}

export default Adidas
