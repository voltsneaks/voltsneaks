import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Reebok = () => {
  return (
      <Container>
        <Productos category="marcas" index="Reebok"/>
      </Container>
  )
}

export default Reebok
