import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Diadora = () => {
  return (
      <Container>
          <Productos category="marcas" index="Diadora"/>
      </Container>
  )
}

export default Diadora
