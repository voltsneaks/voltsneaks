import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Gorras = () => {
  return (
    <Container>
      <Productos category="accesorios" index="gorras"/>
    </Container>
  )
}

export default Gorras
