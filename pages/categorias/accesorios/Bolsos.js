import { Productos } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'

const Bolsos = () => {
  return (
    <Container>
      <Productos category="accesorios" index="bolsos"/>
    </Container>
  )
}

export default Bolsos
