import { Text } from 'components/atoms'
import { Container } from 'components/templates/Layout/Layout.elements'
import styled from 'styled-components'

export const TextBody = styled.div`
  width: 70%;
  margin: 0 auto;
  word-wrap: break-word;
`
export const Title = styled.p`
  font-size: 3rem;
  font-weight: bold;
  margin-bottom: 5rem;
  `
export const Subtitle = styled.p`
  font-size: ${({ big }) => big ? '1.8rem' : '1.6rem'};
  font-weight: bold;
  margin: 3rem 0;
`

const terminos = () => {
  return (
    <Container>
      <TextBody>

        <Title>Términos y Condiciones</Title>

        <Subtitle>TÉRMINOS Y CONDICIONES PARA COMPRAS EN WWW.VOLTSNEAKS.COM Y SU USO POR TODO USUARIO O CONSUMIDOR</Subtitle>

        <Subtitle big>1. ASPECTOS GENERALES. </Subtitle>
        <Text margin="2rem 0">Estos Términos y Condiciones regulan, se aplican y se entienden formar parte de todos los actos y contratos que se ejecutan o celebran mediante los sistemas de oferta y comercialización comprendidos en el sitio web http://www.voltsneaks.com entre sus Usuarios y Deportes Voltsneaks Ltda. (en adelante “Voltsneaks” o “el Proveedor”), de conformidad a las leyes chilenas, a los criterios de buenas prácticas en materia de comercio electrónico y a los estándares generalmente aceptados por el comercio electrónico.</Text>
        <Text margin="2rem 0">En concordancia con lo dispuesto en los artículos 1 y 2 a) de la Ley N° 19.496, para que dichos actos y contratos queden regidos por estos Términos y Condiciones se requiere que se cumplan los siguientes dos requisitos copulativos: i) perfeccionamiento de un acto jurídico que, de conformidad a lo preceptuado en el Código de Comercio u otras disposiciones legales, tenga el carácter de mercantil para el proveedor y de civil para el consumidor; y ii) que dicha venta o acto jurídico oneroso permita al Usuario adquirir, utilizar o disfrutar, como destinatario final, bienes o servicios provistos por Voltsneaks.</Text>
        <Text>El Usuario gozará de los derechos que le reconoce la ley chilena, y de los que le otorgan estos Términos y Condiciones.</Text>

        <Subtitle big>2. REPRESENTANTE DEL PROVEEDOR.</Subtitle>
        <Text>Para facilitar el envío de cualquier presentación, consulta o reclamo sobre el uso del sitio web http://www.voltsneaks.com o las transacciones que en él se realicen, y sin perjuicio de las facultades de sus demás gerentes y apoderados, el Proveedor designa como representante especial a don Fernando Matthei Rubio, jefe de tienda online de Deportes Voltsneaks, disponible en el correo electrónico servicioalcliente@voltsneaks.com; en el número telefónico +56 22 666 5924; y en Avenida Presidente Kennedy N° 5682, comuna de Vitacura, Santiago.</Text>

        <Subtitle big>3. DERECHOS ESPECIALES DEL USUARIO.</Subtitle>
        <Subtitle>3.1. Derecho a registrar una clave secreta</Subtitle>
        <Text margin="2rem 0">El Usuario podrá registrar una contraseña o clave secreta para contratar o efectuar pagos en el sitio web http://www.voltsneaks.com, o solicitar información o acceder a promociones especiales, para lo cual deberá suscribir el formulario que al efecto se contenga en el sitio y enviarlo a Voltsneaks, digitando un "click" en el elemento respectivo. Esta contraseña es requisito para contratar en el referido sitio y permite un acceso personalizado, confidencial y seguro.</Text>
        <Text margin="2rem 0">El Usuario es responsable de mantener la confidencialidad de su clave secreta registrada en el sitio web http://www.voltsneaks.com. Dicha clave es de uso personal e intransferible, y su entrega a terceros no involucra responsabilidad de ninguna especie para Voltsneaks, en caso de mala utilización.</Text>
        <Text margin="2rem 0">El Usuario podrá cambiar su clave, siguiendo el procedimiento establecido en el sitio web http://www.voltsneaks.com.</Text>

        <Subtitle>3.2. Derecho a consultar libremente ofertas</Subtitle>
        <Text margin="2rem 0">La sola visita al sitio web http://www.voltsneaks.com., en el cual se ofrecen determinados bienes y el acceso a determinados servicios, no impone al Usuario obligación alguna, a menos que haya expresado en forma inequívoca y mediante actos positivos su voluntad de aceptar una determinada oferta de Voltsneaks, en la forma indicada en el numeral 7 de estos Términos y Condiciones.</Text>

        <Subtitle>3.3. Derecho a formular consultas y recibir atención al cliente</Subtitle>
        <Text margin="2rem 0">Todo Usuario tiene derecho a formular las consultas que estime necesarias respecto de los productos ofrecidos por Voltsneaks, compras ya realizadas, despacho de productos, garantías, servicio técnico, etc.</Text>
        <Text margin="2rem 0">El horario de Atención al Cliente se extenderá de lunes a viernes desde las 08:00 horas y hasta las 20:00 horas, excepto festivos, en el número telefónico +56 22 666 5924 y en el correo electrónico servicioalcliente@voltsneaks.com. </Text>

        <Subtitle>3.4. Derecho a la protección de sus datos personales</Subtitle>
        <Text margin="2rem 0">Los datos personales que entregue el Usuario sólo podrán ser utilizados por Voltsneaks para perfeccionar contratos, recibir pagos y mejorar la labor de información y comercialización de los productos y servicios con el Usuario. Dicha autorización de uso será revocable por el Usuario.</Text>
        <Text margin="2rem 0">De conformidad a lo anterior, los datos personales que entregue el Usuario sólo podrán ser utilizados por Voltsneaks para: (a) promocionar y ofrecer nuevos productos y servicios, o bien, nuevos atributos, modalidades o características a los productos y servicios que ya están a disposición de los clientes; (b) completar automáticamente los documentos asociados a las transacciones que el Usuario realice sobre la base de los productos adquiridos y/o servicios contratados, o que en el futuro adquiera o contrate con Voltsneaks; (c) ajustar la oferta de productos y servicios al perfil de cliente del Usuario, o bien, efectuar análisis, reportes y evaluaciones a su respecto; y (d) desarrollar acciones comerciales o servicios de post venta, de carácter general o dirigidas personalmente al Usuario, tendientes a mejorar su experiencia como cliente.</Text>
        <Text margin="2rem 0">Al respecto, dentro de los datos personales que Voltsneaks requerirá del Usuario se encuentra su nombre, apellido, compañía, Rut, domicilio, teléfono fijo, celular y correo electrónico. Adicionalmente, para estos efectos constituyen “datos personales” del Usuario los datos de geolocalización, uso y visita del sitio web, historial de navegación, hábitos de compra, entre otros.</Text>
        <Text margin="2rem 0">El Usuario autoriza expresamente a Voltsneaks, de conformidad al artículo 4° de la Ley N°19.628, a efectuar el tratamiento de dichos datos personales únicamente para las finalidades antes señaladas. Voltsneaks siempre será responsable del cumplimiento efectivo de dicha obligación y en caso de que el tratamiento de datos se efectúe por empresas proveedoras de servicios para Voltsneaks, dichas empresas proveedoras de servicios deberán asumir compromisos de confidencialidad y adoptar medidas que aseguren el debido cumplimiento de las obligaciones que impone la Ley 19.628 y la legislación vigente sobre protección de datos personales.</Text>
        <Text margin="2rem 0">El Usuario dispondrá siempre de los derechos de información, rectificación y cancelación de sus datos personales conforme a la Ley 19.628 sobre protección de datos de carácter personal. Para ejercer esos derechos, efectuar dicha revocación o modificar la información personal registrada en el sitio web http://www.voltsneaks.com., el Usuario debe enviar un correo electrónico a servicioalcliente@voltsneaks.com  o comunicarse al número telefónico +56 22 666 5924 en el horario antes mencionado.</Text>
        <Text margin="2rem 0">Para el adecuado resguardo de los datos personales proporcionados por el Usuario, Voltsneaks cuenta con la tecnología SSL (Secure Sockets Layer), la que permite asegurar tanto la autenticidad del sitio, como el cifrado de toda la información que el Usuario entrega a Voltsneaks.</Text>
        <Text margin="2rem 0">Los certificados SSL protegen todos los datos personales que el Usuario entregue a Voltsneaks, incluyendo los de sus tarjetas bancarias, información de su identidad, historial de navegación y preferencias de compras, asegurando de manera fehaciente que éstos no serán divulgados, e impidiendo, al mismo tiempo, el acceso indebido a dicha información por parte de terceros.</Text>

        <Subtitle>3.5. Derecho a cambios</Subtitle>
        <Text margin="2rem 0">Si el producto comprado fue recibido dañado o incompleto, el Usuario puede cambiarlo dentro de los 10 días siguientes a la fecha de recepción, en cualquier tienda Voltsneaks o, si fue despachado a domicilio, podrá solicitar que sea retirado del mismo lugar sin ningún costo adicional.</Text>
        <Text margin="2rem 0">Si el Usuario optare por cambiarlo en una tienda Voltsneaks, debe dirigirse a cualquiera de éstas, donde, además, se atenderán todas sus consultas.</Text>
        <Text margin="2rem 0">Si el producto fue despachado a domicilio y el Usuario prefiere que Voltsneaks lo retire, deberá escribir al correo electrónico servicioalcliente@voltsneaks.com o comunicarse con el Servicio de Atención al Cliente al +56 22 666 5924.</Text>
        <Text margin="2rem 0">El producto debe ser devuelto sin uso, con todos sus embalajes originales y en perfectas condiciones, con los certificados de garantía, manuales de uso, accesorios y regalos promocionales que estuvieron asociados a la compra. El Usuario deberá, además, presentar su boleta, orden de despacho o ticket de cambio.</Text>
        <Text margin="2rem 0">Los técnicos autorizados de Voltsneaks podrán revisar que el producto no presenta fallas o daños imputables al consumidor. Voltsneaks tendrá la opción de enviar un verificador al domicilio, antes de hacer el cambio o retiro desde el domicilio.</Text>
        <Text margin="2rem 0">Si para un producto se recomienda su armado o instalación por un servicio autorizado por el fabricante o por Voltsneaks, el daño o deterioro no debe ser atribuible a un armado o instalación deficiente, caso en el cual simplemente aplicarán las garantías pertinentes.</Text>
        <Text margin="2rem 0">Si el Usuario quiere más información puede escribir a servicioalcliente@voltsneaks.com o comunicarse con el Servicio de Atención al Cliente al +56 22 666 5924.</Text>

        <Subtitle>3.6. Derecho a garantía legal por producto defectuoso </Subtitle>
        <Text margin="2rem 0">Si el producto comprado llegare a presentar deficiencias de fabricación, de materiales, partes, piezas, elementos, de estructura o de calidad, de tal forma que dicho producto no resulte apto para el uso o consumo al que está destinado o fuere claramente diferente al que Voltsneaks hubiese señalado en su publicidad, el Usuario tiene derecho a solicitar, a su opción y dentro de los 3 meses siguientes a la fecha de recepción, una de las siguientes alternativas: i) la reparación gratuita del producto; ii) su reemplazo, previa restitución del producto defectuoso; y iii) la devolución de la cantidad pagada, previa restitución del producto defectuoso. El Usuario deberá, además, presentar su boleta, orden de despacho o ticket de cambio.</Text>
        <Text margin="2rem 0">Antes de proceder con la reparación del producto, su reemplazo o la devolución de la cantidad pagada, los técnicos autorizados de Voltsneaks podrán revisar que el producto no presenta fallas o daños imputables al consumidor. Para dichos efectos, Voltsneaks tendrá la opción de enviar un verificador al domicilio del Usuario.</Text>
        <Text margin="2rem 0">Si el Usuario quiere más información puede escribir a servicioalcliente@voltsneaks.com o comunicarse con el Servicio de Atención al Cliente al +56 22 666 5924.</Text>
        <Text margin="2rem 0">La garantía legal antes señalada es sin perjuicio de otras garantías adicionales que pudieren ser otorgadas por los fabricantes o por Voltsneaks, las que se informarán al Usuario para cada caso particular y según producto específico.</Text>

        <Subtitle big>4. OBLIGACIONES ESPECIALES DEL PROVEEDOR.</Subtitle>
        <Text margin="2rem 0">En los contratos ofrecidos en el sitio web http://www.voltsneaks.com, el Proveedor informará, en forma inequívoca y fácilmente accesible: los pasos que deben seguirse para celebrarlos; su dirección de correo electrónico y los medios técnicos a disposición del Usuario para identificar y corregir errores en el envío o en sus datos; y, si el documento electrónico en que se formalice el contrato fuera archivado, cómo será accesible al Usuario. Estas obligaciones se entenderán cumplidas por el hecho de seguir el Usuario los pasos que se indican en el sitio para hacer una compra o contratar un servicio.</Text>

        <Subtitle big>5. DOCUMENTO TRIBUTARIO ELECTRÓNICO.</Subtitle>
        <Subtitle>Documento tributario electrónico.</Subtitle>
        <Text margin="2rem 0">El Usuario, en su calidad de receptor manual de documentos electrónicos, de conformidad con la Resolución Exenta número 11 del 14 de febrero de 2003 emitida por el Servicio de Impuestos Internos (que establece el procedimiento para que contribuyentes autorizados para emitir documentos electrónicos puedan también enviarlos por estos medios a receptores manuales), autoriza a Deportes Voltsneaks Ltda., RUT N° 76.074.938-9, para que el documento tributario correspondiente a la transacción que efectúe en su calidad de comprador, pueda serle entregado por un medio electrónico, mediante su envío a la casilla de correo electrónico que el Usuario informe y/o registre en el sitio web http://www.voltsneaks.com. Asimismo, autoriza a Voltsneaks para que el aviso de publicación del documento tributario, le sea notificado del mismo modo antes señalado.</Text>

      </TextBody>
    </Container>
  )
}

export default terminos
