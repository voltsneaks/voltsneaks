import { ProductGallery } from 'components/molecules'
import { ProductDetails } from 'components/organisms'
import { Container } from 'components/templates/Layout/Layout.elements'
import Head from 'next/head'
import styled from 'styled-components'
import theme from 'styles/theme'

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  margin: 5rem 0;

  @media ${theme.breakpoints.mobile}{
    flex-direction: column;
    margin-bottom: 2rem;

  }
`

const Producto = ({ product }) => {
  return (
    <>
    <Head>
      <title>{product.marca.nombre} | {product.modelo}</title>
    </Head>
    <Container>
      <Wrapper>
        <ProductGallery imagenes={product.imagenes}/>
        <ProductDetails {...product}/>
      </Wrapper>
    </Container>
    </>
  )
}

export async function getStaticPaths () {
  const data = await fetch('https://voltsneaks-api.herokuapp.com/products?_limit=-1')
  const res = await data.json()

  const paths = res.map(prod => ({
    params: { id: prod.id }
  }))

  return { paths, fallback: false }
}

export async function getStaticProps (context) {
  const data = await fetch(`https://voltsneaks-api.herokuapp.com/products/${context.params.id}`)
  const product = await data.json()
  return {
    props: { product },
    revalidate: 1
  }
}

export default Producto
