export const query = `
query {
    products {
      marca {
        nombre
      }
      categorias {
        nombre
      }
      numeros {
        numero
      }
      imagenes {
        formats
      }
      modelo
      descripcion
      precio
      cantidad
      disponible
      id
    }
  }
`
