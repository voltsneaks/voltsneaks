import * as Yup from 'yup'

export const SignupSchema = Yup.object().shape({
  nombre: Yup.string().min(2, 'El nombre es muy corto').max(50, 'El nombre es muy largo').required('Este campo es obligatorio'),
  apellido: Yup.string().min(2, 'El apellido es muy corto').max(50, 'El apellido es muy largo').required('Este campo es obligatorio'),
  rut: Yup.string().min(8, 'El Rut esta incompleto').max(10, 'El Rut es invalido').required('Este campo es obligatorio'),
  email: Yup.string().email('El email ingresado no es valido').required('Este campo es obligatorio'),
  password: Yup.string().required('Este campo es obligatorio').matches(/^(?=.*[A-Za-z])[A-Za-z\d@$!%*#?&]{6,}$/, 'Debe contener al menos 6 caracteres')
})
export const logInSchema = Yup.object().shape({
  email: Yup.string().email('El email ingresado no es valido').required('Este campo es obligatorio'),
  password: Yup.string().required('Este campo es obligatorio').matches(/^(?=.*[A-Za-z])[A-Za-z\d@$!%*#?&]{6,}$/, 'Debe contener al menos 6 caracteres')
})
export const CheckoutSchema = Yup.object().shape({
  email: Yup.string().email('El email ingresado no es valido').required('Este campo es obligatorio'),
  nombre: Yup.string().required('Este campo es obligatorio').min(2, 'El nombre es muy corto').max(50, 'El nombre es muy largo'),
  apellido: Yup.string().required('Este campo es obligatorio').min(2, 'El apellido es muy corto').max(50, 'El apellido es muy largo'),
  direccion: Yup.string().required('Este campo es obligatorio').min(2, 'La direccion no es valida').max(50, 'La direccion es muy larga'),
  ciudad: Yup.string().required('Este campo es obligatorio').min(2, 'El nombre de la ciudad es invalido').max(30, 'El nombre de la ciudad es invalido'),
  zip: Yup.string().required('Este campo es obligatorio').min(7, 'El codigo ingresado es invalido').max(7, 'El codigo ingresado es invalido'),
  telefono: Yup.string().required('Este campo es obligatorio').matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, 'El numero ingresado no es valido')
})
// export const SignupSchema = Yup.object().shape({
//     nombre: Yup.string().min(2, 'El nombre es muy corto').max(50, 'El nombre es muy largo').required('Este campo es obligatorio'),
//     apellido: Yup.string().min(2, 'El apellido es muy corto').max(50, 'El apellido es muy largo').required('Este campo es obligatorio'),
//     rut: Yup.number().min(8, 'El Rut esta incompleto').max(10, 'El Rut es invalido').required('Este campo es obligatorio'),
//     email: Yup.string().email('El email ingresado no es valido').required('Este campo es obligatorio'),
//     password: Yup.string().required('Este campo es obligatorio').matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,"Debe contener al menos 8 caracteres, una mayuscula, una minuscula, un numero y un caracter especial"),
//     confirmPassword: yup.string().required().oneOf([yup.ref("password"), null], "Passwords must match")
// })
// export const logInSchema = Yup.object().shape({
//     email: Yup.string().email('El email ingresado no es valido').required('Este campo es obligatorio'),
//     password: Yup.string().required('Este campo es obligatorio').matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,"Debe contener al menos 8 caracteres, una mayuscula, una minuscula, un numero y un caracter especial"),
// })
