export const marcas = [

  {
    id: 1,
    value: 'Adidas'
  },
  {
    id: 2,
    value: 'Nike'
  },
  {
    id: 3,
    value: 'Converse'
  },
  {
    id: 4,
    value: 'Reebok'
  },
  {
    id: 5,
    value: 'Diadora'
  },
  {
    id: 6,
    value: 'NewBalance'
  },
  {
    id: 7,
    value: 'Puma'
  }

]

export const calzado = [
  {
    id: 11,
    value: 'Baloncesto'
  },
  {
    id: 12,
    value: 'Running'
  },
  {
    id: 13,
    value: 'Training'
  },
  {
    id: 14,
    value: 'Casual'
  }
]

export const accesorios = [
  {
    id: 15,
    value: 'Bolsos'
  },
  {
    id: 16,
    value: 'Gorras'
  }
]
