import { query } from 'services/query'
import { useState, createContext, useEffect, useContext } from 'react'

/// ////////////// Export Context /////////////////
export const DataContext = createContext()
export const useData = () => useContext(DataContext)

/// ////////////// Component Function /////////////////
export const DataProvider = ({ children }) => {

  const [data, setData] = useState([])
  const [productos, setProductos] = useState([])
  const [carrito, setCarrito] = useState([])

  /// ////////////// Graphql Fetch Data /////////////////
  useEffect(() => {
    fetch('https://voltsneaks-api-store.herokuapp.com/graphql', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: query })
    })
      .then(res => res.json())
      .then(data => {
        setData(data.data.products)
        if (Object.keys(productos).length === 0) {
          setProductos(data.data.products)
        }
      })
  }, [])

  /// ////////////// Local Storage /////////////////
  useEffect(() => {
    const dataCart = JSON.parse(localStorage.getItem('dataCart'))
    if (dataCart) setCarrito(dataCart)
  }, [])

  useEffect(() => {
    localStorage.setItem('dataCart', JSON.stringify(carrito))
  }, [carrito])

  /// ////////////// Add Product To Cart /////////////////
  const agregarProd = (prod) => {
    // let check = carrito.every((item) => item.id !== prod.id);
    // let data = productos.filter((product) => product.id === prod.id);
    // if (check) {
    return setCarrito([...carrito, prod])
    // } else {
    //   alert("El producto ya se encuentra en la bolsa.");
    // }
  }

  /// ////////////// Remove Product From Cart /////////////////
  const eliminarProd = (id) => {
    const filtro = carrito.filter((prod) => prod.id !== id)
    setCarrito(filtro)
  }

  /// ////////////// Increase Quantity /////////////////
  const aumentar = (id) => {
    carrito.map((item) => {
      if (item.id === id) {
        item.cantidad += 1
      }
    })
    setCarrito([...carrito])
  }

  /// ////////////// Decrease Quantity /////////////////
  const disminuir = (id) => {
    carrito.map((item) => {
      if (item.id === id) {
        item.cantidad === 1 ? (item.cantidad = 1) : (item.cantidad -= 1)
      }
    })
    setCarrito([...carrito])
  }

  const checkoutSend = (email, nombre, apellido, direccion, ciudad, telefono) => {

  }

  /// ////////////// Object To Export /////////////////
  const value = {
    data,
    productos,
    setProductos,
    carrito,
    agregarProd,
    eliminarProd,
    aumentar,
    disminuir,
    checkoutSend
  }

  return (<DataContext.Provider value={value}>
                {children}
          </DataContext.Provider>)
}
